﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
   
    // Start is called before the first frame update
    void Awake()
    {
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    public void RunSound()
    {
        int r = UnityEngine.Random.Range(0, 3);
        if(r  == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "step0");
            s.source.Play();
        }
        if (r == 1)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "step1");
            s.source.Play();
        }
        if (r == 2)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "step2");
            s.source.Play();
        }

    }

    public void WalkSound()
    {
        int r = UnityEngine.Random.Range(0, 2);
        if (r == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "walk0");
            s.source.Play();
        }
        if (r == 1)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "walk1");
            s.source.Play();
        }
        //if (r == 2)
        //{
        //    Sound s = Array.Find(sounds, sound => sound.name == "step2");
        //    s.source.Play();
        //}

    }

    public void WooshSound()
    {
        int r = UnityEngine.Random.Range(0, 3);
        if (r == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "woosh0");
            s.source.Play();
        }
        if (r == 1)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "woosh1");
            s.source.Play();
        }
        if (r == 2)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "woosh2");
            s.source.Play();
        }
    }

    public void HitSound()
    {
        Sound s = Array.Find(sounds, sound => sound.name == "hit0");
        s.source.Play();
    }

    public void SlashSound()
    {
        int r = UnityEngine.Random.Range(0, 2);
        if (r == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "slash0");
            s.source.Play();
        }
        if (r == 1)
        {
            Sound s = Array.Find(sounds, sound => sound.name == "slash1");
            s.source.Play();
        }
    }
}
