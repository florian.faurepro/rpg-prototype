﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float rotationSpeed;
    public bool isActiveBagClosed = true;
    public bool free = true;
    public Transform target, player;
    public float mouseX, mouseY;
    public float JoystickX, JoystickY;

    public GamePadDetection gamePad;

    public Transform lockTarget;

    private void Start()
    {
        // Cursor.lockState = CursorLockMode.Locked;
        gamePad = GamePadDetection.instance;        
    }

    private void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if(isActiveBagClosed) CameraControl();
    }

    public void CameraControl()
    {
        // gamepad control
        if (gamePad.Xbox_One_Controller != 0 || gamePad.PS4_Controller != 0)
        {
        JoystickX += Input.GetAxis("JoystickX") * rotationSpeed;
        JoystickY += Input.GetAxis("JoystickY") * rotationSpeed / 2;
        JoystickY = Mathf.Clamp(JoystickY, -30, 30);

            if (free)
            {
                transform.LookAt(target);
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    target.rotation = Quaternion.Euler(JoystickY, JoystickX, 0);
                }
                else
                {
                    target.rotation = Quaternion.Euler(JoystickY, JoystickX, 0);
                    if (player.GetComponent<PlayerStats>().isAlive == true)
                        player.rotation = Quaternion.Euler(0, JoystickX, 0);
                }
            }
            else
            {
                player.LookAt(lockTarget);
            }
        }
        // mouse control
        else if (gamePad.miceNKeys == 1)
        {
            mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
            mouseY += Input.GetAxis("Mouse Y") * rotationSpeed / 2;
            mouseY = Mathf.Clamp(mouseY, -30, 10);

            if (free)
            {
                transform.LookAt(target);
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    target.rotation = Quaternion.Euler(-mouseY, mouseX, 0);
                }
                else
                {
                    target.rotation = Quaternion.Euler(-mouseY, mouseX, 0);
                    player.rotation = Quaternion.Euler(0, mouseX, 0);                   
                }
            }
            else
            {
                player.LookAt(lockTarget);
            }
        }
    }

    public void CameraLock(Transform targetLock)
    {
        free = (!free);
        lockTarget = targetLock;
    }

    public void CameraSwitch(Transform targetLock)
    {
        lockTarget = targetLock;
    }
}