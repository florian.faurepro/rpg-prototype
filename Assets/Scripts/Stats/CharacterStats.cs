﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public float maxHealth;
    public float maxStamina;
    public int protection;
    public int monney;
    public float attackRateMin = 1f;
    public float attackRateMax = 5f;
    public float energy;
    public float energyRegenRate = 10f;
    public float healthRegenRate = 0.1f;

    // stuf values
    public Stat damage;
    public Stat armor;
    public Stat weaponSpeed;

    // stats
    public Stat vitality;
    public Stat stamina;
    public Stat strength;
    public Stat agility;
    public Stat dexterity;
    public Stat intelligence;
    public Stat spirit;
    

    // caracs (computed)
    public float critChances { get; protected set; }
    public double attackRate { get; protected set; }
    public float attackSpeed { get; protected set; }
    public int attackPower { get; set; }
    public int damageOutput { get; protected set; }
    public float currentHealth { get; set; }
    public float currentStamina { get; set; }
    public double memory { get; protected set; }

    private void Awake()
    {
        InitiateStats();
    }

    private void Update()
    {

    }

    // calculates the caracteistics from the entity statistics
    public virtual void UpdateStats()
    {        
        maxHealth = vitality.GetValue() * 5;
        maxStamina = stamina.GetValue() * 3;
        protection = (strength.GetValue() / 3) + armor.GetValue();
        attackPower = strength.GetValue() + (agility.GetValue() / 2) + dexterity.GetValue();
        critChances = (strength.GetValue() / 5) + (agility.GetValue() / 3) + (dexterity.GetValue() / 2); 
        damageOutput = attackPower + damage.GetValue();
        attackRate = strength.GetValue() + agility.GetValue() + dexterity.GetValue();
        attackRate /=  66;

        attackSpeed = weaponSpeed.GetValue();
    }

    // calculates the caracteistics from the entity statistics then sets the health to 100%
    public virtual void InitiateStats()
    {
        maxHealth = vitality.GetValue() * 5;
        maxStamina = stamina.GetValue() * 3;
        protection = (strength.GetValue() / 3) + armor.GetValue();
        attackPower = strength.GetValue() + (agility.GetValue() / 2) + dexterity.GetValue();
        critChances = (strength.GetValue() / 5) + (agility.GetValue() / 3) + (dexterity.GetValue() / 2);
        damageOutput = attackPower + damage.GetValue();
        attackRate = strength.GetValue() + agility.GetValue() + dexterity.GetValue();
        memory = intelligence.GetValue() + spirit.GetValue();

        currentHealth = maxHealth;
        currentStamina = maxStamina;
    }

    //public void CapHealthAndEnergy()
    //{
    //    if (currentHealth > maxHealth)
    //        currentHealth = maxHealth;
    //    if (currentStamina > maxStamina)
    //        currentStamina = maxStamina;
    //}

    // ment to be overwritten
    public virtual void Die()
    {
       
    }
}
