﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;



public class PlayerCombat : MonoBehaviour
{
    #region Variables
    public Animator animator;
    public EquipmentManager equipment;

    // attack check variables
    public Transform attackPoint;
    public float attackRange = 1f;
    public LayerMask enemyLayers;

    public AudioManager sound;

    // player stats
    public CharacterStats damage;
    public int hitpoints;
    public float crit;
    public bool isCrit;
    public bool isMagic;
    public int critRoll;
    public double attackRate;
    public double attackSpeed;
    public double nextAttack;
    public double resetAttackTime;
    public int comboStep;
    public bool isBlocking = false;

    public InventoryUI inventory;

    // targetting-related variables
    public int targettingRange;
    public CameraController lockCamera;
    public List<float> distances = new List<float>();
    IDictionary<float, Transform> enemiesTargets = new Dictionary<float, Transform>();
    public Transform enemyToLock;
    public Transform lastLocked;
    public int nextEnemyToLock;
    public float timeUntilNextSwitch;
    GameObject[] defenseTris;

    // input right joystick
    public float prevTarget;
    #endregion

    private void Start()
    {
        equipment = EquipmentManager.instance;
        lockCamera = GetComponentInChildren<CameraController>();
    }

    void Update()
    {
        // checks if target is still alive and in range
        CheckLockability();

        // listen for attacks inputs
        if(Input.GetButtonDown("AttackLight"))
        {
            attackSpeed = PlayerStats.instance.attackSpeed;

            // prevents from attacking with open bags / character sheet
            if (!inventory.bagOpen && !inventory.characterSheetOpen)
            {
                // prevents from attacking by clicking on inventory
                if (EventSystem.current.IsPointerOverGameObject())
                    return;
                
                // checks for attack cooldowns, attacks if clear
                if (Time.time >= nextAttack)
                    AttackLight();
            }
        }

        // checks if it is tie to reset the combo
        if (Time.time > resetAttackTime && comboStep != 0) 
            ResetCombo();

        // listen for lock input
        if (Input.GetButtonUp("LockTarget"))
        {
            // checks if a target is registered, if not tries to lock one
            if (enemyToLock == null)
                SelectEnemyToLock();
            
            // or unlocks if there is one
            else
                Unlock();
        }

        // gets the left stick input side
        prevTarget = Input.GetAxis("PrevTarget");
        if (prevTarget > 0 && enemyToLock != null || prevTarget < 0 && enemyToLock != null)
        {
            if (Time.time > timeUntilNextSwitch)
                NextEnemyToLock();
        }

        if (Input.GetButtonDown("GuardNMagic"))
            Block();

        if (Input.GetButtonUp("GuardNMagic"))
            Block();
    }

    #region Attack
    // puts an end to the current combo
    public void ResetCombo()
    {
        // puts all combo - related bools to false
        animator.SetBool("AttackL1", false);
        animator.SetBool("AttackL2", false);
        animator.SetBool("AttackL3", false);
        animator.SetBool("inCombo", false);

        // set the combo step to initial value
        comboStep = 0;
    }

    void AttackLight()
    {
        if (damage.currentStamina >= 5)
        {
            // Choose attack depending on comboStep and type of weapon
            if (equipment.isArmed1H == true && comboStep != 3 || equipment.isArmed2H == true && comboStep != 3)
            {
                // checks the cooldowns and combosteps
                if (comboStep == 0 && Time.time >= nextAttack)
                    AttackLight1();
                if (comboStep == 1 && Time.time <= resetAttackTime && Time.time >= nextAttack)            
                    AttackLight2();
                if (comboStep == 2 && Time.time <= resetAttackTime && Time.time >= nextAttack)            
                    AttackLight3();            
            } 
            //if (equipment.isArmed2H == true && comboStep != 3)
            //{

            //}
            else
            {
                animator.SetTrigger("UnarmedAttack");

                // sets timer until next attack
                nextAttack = Time.time + 1f;
            }            
        }
    }

    public void AttackLight1()
    {
        // sets the combo-related vars
        animator.SetBool("inCombo", true);
        animator.SetBool("AttackL1", true);

        // sets timer until next attack
        if (equipment.isArmed1H)
        {
            // set time until next attack
            nextAttack = Time.time + .7f / attackSpeed;
            Debug.Log("AttackSpeed : " + attackSpeed + "next attack : " + nextAttack);

            // set time until combo reset
            resetAttackTime = Time.time + 1f / (attackSpeed /2);
            Debug.Log(resetAttackTime);

            // spends energy
            damage.currentStamina -= 15;
        }
        if (equipment.isArmed2H)
        {
            // set time until next attack
            nextAttack = Time.time + 1.3f / attackSpeed;

            // set time until combo reset
            resetAttackTime = Time.time + 1f / (attackSpeed/2);

            // spends energy
            damage.currentStamina -= 25;
        }

        // step up the combo
        comboStep++;

    }

    public void AttackLight2()
    {
        //Debug.Log("attack2");

        // sets the combo-related vars
        animator.SetBool("AttackL1", false);
        animator.SetBool("AttackL2", true);

        if (equipment.isArmed1H)
        {
            // set time until next attack
            nextAttack = Time.time + .5f / (attackSpeed/2);

            // set time until combo reset
            resetAttackTime = Time.time + 1f / (attackSpeed/2);

            // spends energy
            damage.currentStamina -= 15;

            comboStep++;
        }
        if (equipment.isArmed2H)
        {
            // set time until next attack
            nextAttack = Time.time + 1.5f / (attackSpeed/2);

            // set time until combo reset
            resetAttackTime = Time.time + 1f / (attackSpeed/2);

            // spends energy
            damage.currentStamina -= 25;
        }
    }

    public void AttackLight3()
    {
        //Debug.Log("attack3");

        // sets the combo-related vars
        animator.SetBool("AttackL2", false);
        animator.SetBool("AttackL3", true);

        // sets timer until next attack
        if (equipment.isArmed1H)
            nextAttack = Time.time + .5f;
        if (equipment.isArmed2H)
            nextAttack = Time.time + 2f;

        // opens the window for another step in the combo
        resetAttackTime = Time.time + 1f / (attackSpeed/2);

        // step up the combo
        comboStep++;

        // spends energy
        damage.currentStamina -= 15;


    }

    #endregion Attack
    public void Damage()
    {
        // updates the damage related variables
        hitpoints = damage.damageOutput;
        crit = damage.critChances;
        attackRate = damage.attackRate;

        // detect enemies
        Collider[] enemiesInRange = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);

        // damage enemy
        foreach (Collider enemy in enemiesInRange)
        {
            // sets the variables
            #region Damage Vars
            float prevDistance = 0;
            string closer = "";
            bool damageFromBack = false;
            GameObject[] defenseTris = new GameObject[3];

            defenseTris[0] = enemy.transform.Find("DefensePointFront").gameObject;
            defenseTris[1] = enemy.transform.Find("DefensePointBackRight").gameObject;
            defenseTris[2] = enemy.transform.Find("DefensePointBackLeft").gameObject;
            #endregion Damage Vars

            for (int i = 0; i < defenseTris.Length; i++)
            {
                float distance = Vector3.Distance(transform.position, defenseTris[i].transform.position);

                if (i == 0)
                {
                    prevDistance = distance;
                    closer = defenseTris[i].name;
                }
                else if (prevDistance > distance)
                {
                    closer = defenseTris[i].name;
                }
            }

            if (closer != "DefensePointFront")
                damageFromBack = true;

            isCrit = IsCrit();

            // Send damage to enemy(ies)
            enemy.GetComponent<EnemyStats>().TakeDamage(hitpoints, isCrit, isMagic, damageFromBack);
            sound.SlashSound();
        }
    }

    public bool IsCrit()
    {
        // get roll score for crit
        critRoll = UnityEngine.Random.Range(0, 100);

        // compares critRoll to critChances to see if crit
        if (critRoll <= crit)
        {
            // double damage in case of critical strike
            hitpoints += hitpoints / 2;
            return true;
        }
        else
            return false;
    }

    public void Block()
    {
        isBlocking = (!isBlocking);
        animator.SetBool("isBlocking", isBlocking);
    }

    #region Lock
    public void SelectEnemyToLock()
    {
        // detect enemies
        Collider[] enemiesInRange = Physics.OverlapSphere(transform.position, targettingRange, enemyLayers);
        foreach(Collider enemy in enemiesInRange)
        {
            if (enemy.GetComponent<EnemyStats>().isAlive)
            {
                // check distances between player and enemies in reach
                float distance = Vector3.Distance(enemy.transform.position, transform.position);

                // adds them in List
                enemiesTargets.Add(distance, enemy.transform);
                distances.Add(distance);
            }
        }

        // creates distances array
        distances.Sort();
        distances.ToArray();

        if (enemiesTargets.Count == 0)
            return;
        // selects the next lock candidate
        enemiesTargets.TryGetValue(distances[0], out enemyToLock);
        Lock(enemyToLock);
        enemyToLock.GetComponent<EnemyStats>().isLocked = true;
        // clears lists
        enemiesTargets.Clear();
        distances.Clear();
    }

    public void NextEnemyToLock()
    {
        nextEnemyToLock ++;
        lastLocked = enemyToLock;

        // detect enemies
        Collider[] enemiesInRange = Physics.OverlapSphere(transform.position, targettingRange, enemyLayers);
        if (enemiesInRange.Length != 0)
        {
            foreach (Collider enemy in enemiesInRange)
            {
                if (enemy.transform != lastLocked && enemy.GetComponent<EnemyStats>().isAlive)
                {
                    // check distances between player and enemies in reach
                    float distance = Vector3.Distance(enemy.transform.position, transform.position);

                    // adds them in List
                    enemiesTargets.Add(distance, enemy.transform);
                    distances.Add(distance);
                }                    
            }
                
            // limits the size of the enemies to lock to 10
            if (nextEnemyToLock >= enemiesTargets.Count) nextEnemyToLock = 0;

            // if only one enemy in range, abort
            if (enemiesTargets.Count == 0)
                return;
       
            // creates distances array
            distances.Sort();
            distances.ToArray();

            // Disables lastLocked enemy's isLocked bool <<<
            lastLocked.GetComponent<EnemyStats>().isLocked = false;

            // selects the next lock candidate
            enemiesTargets.TryGetValue(distances[nextEnemyToLock], out enemyToLock);

            // sets the isLocked bool and player rotation
            enemyToLock.GetComponent<EnemyStats>().isLocked = true;
            SwitchLock(enemyToLock);

            // enables the new target's healthBar UI then clears lists
            enemiesTargets.Clear();
            distances.Clear();

            timeUntilNextSwitch = Time.time + 0.3f;
        }        
    }

    public void Lock(Transform enemyToLock)
    {            
        // locks camera then activates healthBar
        lockCamera.CameraLock(enemyToLock);
        enemyToLock.GetComponent<EnemyUIElements>().Activate();
    }

    public void SwitchLock(Transform enemyToLock)
    {
        // disactivates last locked enemy's healthBar
        lastLocked.GetComponent<EnemyUIElements>().Activate();

        // locks camera then activates healthBar
        lockCamera.CameraSwitch(enemyToLock);
        enemyToLock.GetComponent<EnemyUIElements>().Activate();
    }

    public void CheckLockability()
    {
        // checks if locked enemy exists and is dead
        if (enemyToLock != null && enemyToLock.GetComponent<EnemyStats>().isAlive == false)
        {
            //creates a list of every enemy available for locking
            Collider[] enemiesInRange = Physics.OverlapSphere(transform.position, targettingRange, enemyLayers);

            // if no enemy in range is available for lock, unlocks
            if (enemiesInRange.Length == 0)
                Unlock();

            // else locks the next available enemy
            else
                NextEnemyToLock();
        }
    }

    public void Unlock()
    {
        // frees the camera then disactivates the last locked enemy's healthBar
        lockCamera.free = true;
        enemyToLock.GetComponent<EnemyUIElements>().Activate();
        enemyToLock.GetComponent<EnemyStats>().isLocked = false;
        enemyToLock = null;
    }

    #endregion Lock

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
        Gizmos.DrawWireSphere(transform.position, targettingRange);
    }
}
