﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMotor : MonoBehaviour
{
    // player related variables
    public float speed;
    float previousSpeed;
    public bool isDiscreet { get; set; }
    public bool mobile;
    public float jumpSpeed;
    public Vector3 playerMovement;
    public float speedPercent;
    float sprintSpeed;
    public  Vector3 prevLocation;
    public Vector3 difference;

    // components
    public Rigidbody rb;
    protected Animator animator;
    public EquipmentManager equipment;
    public PlayerStats player;
    public PlayerCombat playerCombat;

    // dodge related variables
    public float dashTimer;
    public float iFrameDelay = .1f;
    public float iFrameDuration = .2f;
    public float dashCooldown = .5f;
    public float actCoolDown;
    public float dashInputH;
    public float dashInputV;
    public bool canDash = true;
    public bool sprint = false;
    public IEnumerator sprintCoroutine;
    public float nominalEnergyRegenRate;


    // sets variables and references
    private void Start()
    {
        isDiscreet = false;
        mobile = true;

        equipment = EquipmentManager.instance;
        animator = GetComponentInChildren<Animator>();
        player = GetComponent<PlayerStats>();
        playerCombat = GetComponent<PlayerCombat>();
        previousSpeed = speed;
        nominalEnergyRegenRate = player.energyRegenRate;
        sprintSpeed = speed * 2;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // gets the player speed percentage
        speedPercent = playerMovement.magnitude * 10;

        // check if player is alive and calls movement method
        if (player.isAlive)
            PlayerMovement();

        // not sure this is useful...
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        // check if the dash cooldown is up
        if (actCoolDown <= 0)
        {
            // listen to input "Dodge"
            // check if player has enough stamina & player is not doing something that prevents him from dashing
            if (Input.GetButtonUp("Dodge") && player.currentStamina > 5 && canDash)
                Dash();
        }
        // if cooldown is not up reduces the remaining time
        else
            actCoolDown -= Time.deltaTime;

        #region Sprint
        // starts the countdown before sprint
        if (Input.GetButtonDown("Dodge"))
        {
            sprintCoroutine = StartSprint(.5f);
            StartCoroutine(sprintCoroutine);
        }

        // if the button is still pressed after the countdown, starts to sprint
        if (Input.GetButton("Dodge") && sprint && player.currentStamina > 5)
        {
            speed = sprintSpeed;
            player.energyRegenRate = -3;
        }
        if (sprint)
        {
            // if the button is released, stop sprinting, reverts the sprint variable, and after a delay re-enables the player to dodge
            if (Input.GetButtonUp("Dodge") || player.currentStamina <= 0.1 || playerMovement.magnitude == 0)
            {
                speed = previousSpeed;
                player.energyRegenRate = nominalEnergyRegenRate;
                sprint = false;
                StopAllCoroutines();
                StartCoroutine(StopSprint(.1f));
            }
        }
        #endregion Sprint

        

        // checks the player's speed and sets discretion accordingly
        if (speedPercent <= 0.5)
            isDiscreet = true;
        else isDiscreet = false;        
    }

    public void PlayerMovement()
    {
        // left Joystick inputs
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        // Animation style
        if (equipment.isArmed1H) animator.SetBool("IsArmed1H", true);
        else animator.SetBool("IsArmed1H", false);

        if (equipment.isArmed2H) animator.SetBool("IsArmed2H", true);
        else animator.SetBool("IsArmed2H", false);


        // Actual movement
        if (mobile == true)
        {
            playerMovement = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
            transform.Translate(playerMovement, Space.Self);
        }        
    }
    
    #region Dash
    public void Dash()
    {
        dashInputH = 0;
        dashInputV = 0;

        // left Joystick inputs
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        int dashSpeed = 20;
        
        if (canDash)
        {
            // dash right
            if (hor > 0)
            {
                rb.AddForce(transform.right * dashSpeed, ForceMode.Impulse);
                animator.SetTrigger("Dash");
                dashInputH ++;
            }
            // dash left
            if (hor < 0)
            {
                rb.AddForce(-transform.right * dashSpeed, ForceMode.Impulse);
                animator.SetTrigger("Dash");
                dashInputH --;
            }
            // dash front
            if (ver > 0)
            {
                rb.AddForce(transform.forward * dashSpeed, ForceMode.Impulse);
                animator.SetTrigger("Dash");
                dashInputV ++;
            }
            // Dash back
            if (ver < 0 || hor == 0 && ver == 0)
            {
                rb.AddForce(-transform.forward * dashSpeed, ForceMode.Impulse);
                animator.SetTrigger("Dash");
                dashInputV--;
            }

            playerCombat.ResetCombo();
            player.currentStamina -= 15;

            actCoolDown = dashCooldown;
            player.IFrame(iFrameDuration, iFrameDelay);

            StopAllCoroutines();
        }
    }

    public void DashAbility()
    {
        canDash = (!canDash);
    }
    #endregion Dash

    IEnumerator StartSprint(float delay)
    {
        yield return new WaitForSeconds(delay);
        //Debug.Log("sprint = true");
        sprint = true;
        canDash = false;
    }

    IEnumerator StopSprint(float delay)
    {
        yield return new WaitForSeconds(delay);
        canDash = true;
    }
}
