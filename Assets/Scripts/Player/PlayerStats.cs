﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : CharacterStats
{
    #region Singleton
    public static PlayerStats instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public GameObject gameOver;
    public GameObject player;
    public PlayerCombat playerCombat;
    public GameObject ui;
    public Animator animator;
    public Slider[] sliders;
    public Slider heatlthSlider;
    public Slider staminaSlider;
    public bool isAlive;
    public bool hasBeenMolestedByGod = false;

    public bool iFrame = false;
    public float iFrameDuration;

    // sets variables and references
    void Start()
    {
        isAlive = true;
        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        ui = GameObject.FindGameObjectWithTag("UI");
        sliders = ui.GetComponentsInChildren<Slider>();
        playerCombat = GetComponent<PlayerCombat>();

        heatlthSlider = sliders[0];
        staminaSlider = sliders[1];

        InitiateStats();
    }

    private void Update()
    {
        // updates the player's health percentage  
        float healthPercent = currentHealth / maxHealth;

        // updates the player's Stamina percentage  
        float staminaPercent = currentStamina / maxStamina;

        // reduce IFrame duration
        if (iFrameDuration > 0)
            iFrameDuration -= Time.deltaTime;

        // populates the healthBar with player's health percentage
        heatlthSlider.value = healthPercent;

        // populates the healthBar with player's health percentage
        staminaSlider.value = staminaPercent;

        Regen();
        ///*CapHealthAndEnergy*/();
    }

    // Add new modifiers on item equip 
    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        // adds new stats when player equips an item
        if(newItem != null)
        {
            armor.AddModifier(newItem.armorModifier);
            damage.AddModifier(newItem.damageModifier);
            vitality.AddModifier(newItem.vitalityModifier);
            stamina.AddModifier(newItem.staminaModifier);
            agility.AddModifier(newItem.agilityModifier);
            strength.AddModifier(newItem.strengthModifier);
            dexterity.AddModifier(newItem.dextertyModifier);
            intelligence.AddModifier(newItem.intelligenceModifier);
            weaponSpeed.AddModifier(newItem.speedModifier);

            UpdateStats();
        }

        // removes old stats when player unequips an item
        if (oldItem != null)
        {
            armor.removeModifier(oldItem.armorModifier);
            damage.removeModifier(oldItem.damageModifier);
            vitality.removeModifier(oldItem.vitalityModifier);
            stamina.removeModifier(oldItem.staminaModifier);
            agility.removeModifier(oldItem.agilityModifier);
            strength.removeModifier(oldItem.strengthModifier);
            dexterity.removeModifier(oldItem.dextertyModifier);
            intelligence.removeModifier(oldItem.intelligenceModifier);
            weaponSpeed.removeModifier(oldItem.speedModifier);

            UpdateStats();
        }
    }

    // calls characterstats method
    // SHOULD BE DELETED TOO
    public  override void UpdateStats()
    {
        base.UpdateStats();        
    }

    // Calculates damage income
    public void TakeDamage(int damage, bool damageFromBack)
    {
        if (iFrameDuration <= 0)
        {           
            // total protection deduced to incomming damage
            damage -= protection;

            // prevents negative damages (heal) from little damage to high armor
            if (damage < 0) damage = 0;

            // deduce final damage from player's health
            // divide damages by 2 and substract half the final damage from the players energy
            if (playerCombat.isBlocking == true && damageFromBack == false)
            {
                damage /= 2;
                currentStamina -= damage / 2;
            }
            currentHealth -= damage;

            Debug.Log(transform.parent.name + " takes " + damage + "(-" + protection + ") damage. " + currentHealth + "HP left.");

            // triggers the hit animation
            if (playerCombat.isBlocking == false)
                animator.SetTrigger("Hit");

            playerCombat.ResetCombo();

            // checks if the player died
            if (currentHealth <= 0)        
                Die();                 
        }
    }

    // adds incoming heal ammount to clamped player's health
    public bool Heal(float healAmmount)
    {
        if (currentHealth < maxHealth)
        {
            currentHealth += healAmmount;
            return true;
        }
        else
            return false;
    }

    // used for debug, heals the player to max health
    public void DivineIntervention()
    {
        if (hasBeenMolestedByGod == false)
        {
            currentHealth = maxHealth;
            hasBeenMolestedByGod = true;
        }
    }

    public void Regen()
    {
        currentStamina += energyRegenRate * Time.deltaTime;
        if (currentStamina >= maxStamina)
            currentStamina = maxStamina;

        currentHealth += healthRegenRate * Time.deltaTime;
        if (currentHealth >= maxHealth)
            currentHealth = maxHealth;
    }

    public void IFrame(float duration, float delay)
    {
        if (delay > 0)
            StartCoroutine(StartIFrame(delay, duration));
        else
            iFrameDuration = duration;
    }

    // renders the player invincible for "duration" second after "delay" second
    IEnumerator StartIFrame(float delay, float duration)
    {
        yield return new WaitForSeconds(delay);
        Debug.Log("Iframe");
        iFrameDuration = duration;
    }

    // trigger death animation and set var accordingly, activates the gameover screen
    public override void Die()
    {
        animator.SetBool("IsAlive", false);
        isAlive = false;
        gameOver.SetActive(true);
    }
}
