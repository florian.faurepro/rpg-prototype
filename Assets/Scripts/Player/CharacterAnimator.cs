﻿using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
    const float LOCOMOTION_SMOOTH_TIME = 0.1f;
    public PlayerMotor motor;
    protected Animator animator;
    public PlayerStats stats;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        motor = GetComponent<PlayerMotor>();
        animator = GetComponentInChildren<Animator>();
        stats = GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPercent = motor.playerMovement.z * 10;
        animator.SetFloat("SpeedPercent", speedPercent, LOCOMOTION_SMOOTH_TIME, Time.deltaTime);

        float horSpeedPercent = motor.playerMovement.x * 10;
        animator.SetFloat("HorSpeedPercent", horSpeedPercent, LOCOMOTION_SMOOTH_TIME, Time.deltaTime);

        float dashInputH = motor.dashInputH;
        float dashInputV = motor.dashInputV;
        animator.SetFloat("DashInputH", dashInputH);
        animator.SetFloat("DashInputV", dashInputV);

        float attackSpeed = stats.attackSpeed;
        animator.SetFloat("AttackSpeed", attackSpeed);
    }
}
