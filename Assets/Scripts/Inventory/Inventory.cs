﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region singleton
    public static Inventory instance;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found !");
            return;
        }
        instance = this;
    }
    #endregion singleton

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public List<Equipment> items = new List<Equipment>();
    public int space = 20;
    public bool Add(Equipment item)
    {
        if (!item.isDefaultItem)
        {
            if(items.Count >= space)
            {
                Debug.Log("Inventory full.");
                return false;
            }
        items.Add(item);

            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }
        return true;
    }

    public void Remove(Equipment item)
    {
        items.Remove(item);

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }
}

// CHANGED ITEM TYPE FROM ITEM TO EQUIPMENT IN BOTH ADD() AND REMOVE() METHODS