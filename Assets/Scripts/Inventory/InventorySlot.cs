﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventorySlot : MonoBehaviour
{
    public Image icon;
    public Image selectedImage;
    public Button removeButton;

    public ObjectDetails objdetails;

    public GameObject detailPanel;
    
    public Equipment item;


    public void AddItem(Equipment newItem)
    {
        item = newItem;

        icon.sprite = item.icon;
        icon.enabled = true;
        //removeButton.interactable = true;
    }

    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;
        //removeButton.interactable = false;
    }

    public void OnRemoveButton()
    {
        Inventory.instance.Remove(item);
    }

    public void UseItem()
    {
        if (item != null)        
            item.Use();        
    }

    public void ShowDetailPanel()
    {
        if (item != null)
        {
            detailPanel.SetActive(true);            
            objdetails.SetItemText(item);
        }
        else
            detailPanel.SetActive(false);
    }
}
