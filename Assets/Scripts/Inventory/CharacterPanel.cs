﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterPanel : MonoBehaviour
{
    #region singleton
    public static CharacterPanel instance;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of character panel found !");
            return;
        }
        instance = this;
    }
    #endregion singleton

    public TextMeshProUGUI currentHealth;
    public TextMeshProUGUI maxHealth;
    public TextMeshProUGUI currentEnergy;
    public TextMeshProUGUI maxEnergy;
    public TextMeshProUGUI attackPower;
    public TextMeshProUGUI criticalChances;
    public TextMeshProUGUI criticalModifier;
    public TextMeshProUGUI memory;

    public TextMeshProUGUI staminaValue;
    public TextMeshProUGUI vitalityValue;
    public TextMeshProUGUI strengthValue;
    public TextMeshProUGUI agilityValue;
    public TextMeshProUGUI dexterityValue;
    public TextMeshProUGUI intelligenceValue;
    public TextMeshProUGUI spiritValue;
    public TextMeshProUGUI attackFlat;
    public TextMeshProUGUI protection;

    [HideInInspector]
    public GameObject player;
    public PlayerStats playerStats;
    public InventoryUI inventoryUI;

    public EquipedSlot[] slot;
    public int space;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<PlayerStats>();
        //slot = GetComponentsInChildren<EquipedSlot>();
        //space = slot.Length;

        RefreshStats();
    }

    private void Update()
    {
        if (gameObject.activeSelf == true && Input.GetButtonUp("AttackLight"))
        {
            Activate();
            inventoryUI.characterSheetOpen = false;
            inventoryUI.InventoryDisplay();
        }

        if (gameObject.activeSelf == true) RefreshStats();
        
        if (gameObject.activeSelf == true && Input.GetButtonUp("Inventory"))
        {
            Activate();
            inventoryUI.characterSheetOpen = false;
        }

    }

    public void Activate()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void Add(Equipment item, int equipslot)
    {
        slot = GetComponentsInChildren<EquipedSlot>();
        space = slot.Length;

        if (!item.isDefaultItem)
        {
            slot[equipslot].AddItem(item);
        }
    }

    // removes the icon from the character panel
    public void Remove(int equipslot)
    {
        slot[equipslot].ClearSlot();
    }
    public void RefreshStats()
    {
        currentHealth.text = playerStats.currentHealth.ToString("f0");
        maxHealth.text = playerStats.maxHealth.ToString();
        currentEnergy.text = playerStats.currentStamina.ToString("f0");
        maxEnergy.text = playerStats.maxStamina.ToString();
        staminaValue.text = playerStats.stamina.GetValue().ToString();
        vitalityValue.text = playerStats.vitality.GetValue().ToString();
        strengthValue.text = playerStats.strength.GetValue().ToString();
        agilityValue.text = playerStats.agility.GetValue().ToString();
        dexterityValue.text = playerStats.dexterity.GetValue().ToString();
        intelligenceValue.text = playerStats.intelligence.GetValue().ToString();
        spiritValue.text = playerStats.spirit.GetValue().ToString();
        attackPower.text = playerStats.attackPower.ToString();
        criticalChances.text = playerStats.critChances.ToString() + "%";
        memory.text = playerStats.memory.ToString();

        attackFlat.text = playerStats.damage.GetValue().ToString();
        protection.text = playerStats.protection.ToString();
    }
}
