﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EquipedSlot : MonoBehaviour
{
    public Image icon;
    public Image selectedImage;
    public Button removeButton;

    public ObjectDetails objdetails;

    public GameObject detailPanel;

    public Equipment item;


    // Clears the slots datas so it doesn't show IN THE INVENTORY
    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    //adds the new item icon in equipment slot 
    public void AddItem(Equipment newItem)
    {
        item = newItem;

        icon.sprite = item.icon;
        icon.enabled = true;
        Debug.Log("equiped in " + gameObject.name + newItem.name + " " + icon.enabled + " " + icon.sprite.name);
        //removeButton.interactable = true;
    }

    // if item exists, executes the item's Use() function...
    public void UseItem()
    {
        if (item != null)        
            item.Use();        
    }

    // show selected item's details
    //public void ShowDetailPanel()
    //{
    //    if (item != null)
    //    {
    //        // activates and populates the element
    //        detailPanel.SetActive(true);            
    //        objdetails.SetItemText(item);
    //    }
    //    else
    //        // disactivates the element
    //        detailPanel.SetActive(false);
    //}
}
