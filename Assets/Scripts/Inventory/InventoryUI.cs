﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public GameObject cameraController;
    public GamePadDetection pad;
    
    public Transform itemParents;
    Inventory inventory;
    InventorySlot[] slots;
    public GameObject inventoryUI;

    public CharacterPanel charPanel;

    public bool bagOpen = false;
    public bool characterSheetOpen = false;

    public float camSpeed;

    public int inventoryPosition = 0;
    public float unlockCrossTime;

    // initiates the variables and stuff
    void Start()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;

        slots = itemParents.GetComponentsInChildren<InventorySlot>();

        cameraController = GameObject.FindGameObjectWithTag("Camera");
        camSpeed = cameraController.GetComponent<CameraController>().rotationSpeed;

        Cursor.visible = bagOpen;
    }

    // Update is called once per frame
    void Update()
    {
        // opens bag
        if (Input.GetButtonDown("Inventory") && !characterSheetOpen)
            InventoryDisplay();        

        // set the cursor position in the bag
        BagNav();

        // use item @ cursorPosition
        if (bagOpen && Input.GetButtonDown("Action"))        
            slots[inventoryPosition].UseItem();
        

        // closes inventory and opens character sheet when left shoulder is pressed
        if (bagOpen && Input.GetButtonDown("GuardNMagic"))
        {
            InventoryDisplay();
            characterSheetOpen = true;
            charPanel.Activate();
        }
    }

    // loops over all the slots to find the first empty one
    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]); 
            } 
            else
            {
                slots[i].ClearSlot(); 
            }
        }
    }

    // displays the inventory
    public void InventoryDisplay()
    {
        // activates the gameObject
        inventoryUI.SetActive(!inventoryUI.activeSelf);

        // update the tracking bool
        bagOpen = (!bagOpen);

        // inverts the cameras isActiveBagClosed bool
        //cameraController.GetComponent<CameraController>().isActiveBagClosed = (!cameraController.GetComponent<CameraController>().isActiveBagClosed);

        // activates the selection image and updates the details panel
        slots[inventoryPosition].selectedImage.gameObject.SetActive(true);
        slots[inventoryPosition].ShowDetailPanel();
    }

    #region bagnav
    // set the cursor position in the bag
    public void BagNav()
    {
        if (bagOpen == true && Input.GetAxisRaw("up") > 0)
        {
            if (Time.time > unlockCrossTime)
            {
                ResetInventoryPosition();
                NavUp();
            }
        }
        if (bagOpen == true && Input.GetAxisRaw("up") < 0)
        {
            if (Time.time > unlockCrossTime)
            {
                ResetInventoryPosition();
                NavDown();
            }
        }
        if (bagOpen == true && Input.GetAxisRaw("right") > 0)
        {
            if (Time.time > unlockCrossTime)
            {
                ResetInventoryPosition();
                NavRight();
            }
        }
        if (bagOpen == true && Input.GetAxisRaw("right") < 0)
        {
            if (Time.time > unlockCrossTime)
            {
                ResetInventoryPosition();
                NavLeft();
            }
        }
    }
    void NavUp()
    {
        unlockCrossTime = Time.time + 0.2f;
        if (inventoryPosition <= 3)        
            inventoryPosition += 16;        
        else        
            inventoryPosition -= 4;        

        slots[inventoryPosition].selectedImage.gameObject.SetActive(true);
        slots[inventoryPosition].ShowDetailPanel();

    }

    void NavDown()
    {
        unlockCrossTime = Time.time + 0.2f;
        if (inventoryPosition >= 16)
            inventoryPosition -= 16;
        else
            inventoryPosition += 4;

        slots[inventoryPosition].selectedImage.gameObject.SetActive(true);
        slots[inventoryPosition].ShowDetailPanel();

    }

    void NavRight()
    {
        unlockCrossTime = Time.time + 0.2f;
        if (inventoryPosition == 3 || inventoryPosition == 7 || inventoryPosition == 11 || inventoryPosition == 19)
            inventoryPosition -= 3;
        else
            inventoryPosition ++;

        slots[inventoryPosition].selectedImage.gameObject.SetActive(true);
        slots[inventoryPosition].ShowDetailPanel();
    }

    void NavLeft()
    {
        unlockCrossTime = Time.time + 0.2f;
        if (inventoryPosition == 0 || inventoryPosition == 4 || inventoryPosition == 8 || inventoryPosition == 16)
            inventoryPosition += 3;
        else
            inventoryPosition --;

        slots[inventoryPosition].selectedImage.gameObject.SetActive(true);
        slots[inventoryPosition].ShowDetailPanel();
    }

    void ResetInventoryPosition()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].selectedImage.gameObject.SetActive(false);
        }
    }
    #endregion
}
        
