﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ObjectDetails : MonoBehaviour
{
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI itemDesc;

    IDictionary<string, int> modifiers = new Dictionary<string, int>();
    public int mod;
    public string desc;
    public int panelSize;

    public Image panel;

    public void SetItemText(Equipment item)
    {
        // resets the panel
        DestroyItemText();

        // sets panel details and size
        itemName.text = item.name;
        if (item.identified)
        {
            itemDesc.text = ModifiersList(item);   
            panel.rectTransform.sizeDelta = new Vector2(200, (100 + panelSize*5));
        }
        else
        {
            itemDesc.text = "Unidentified";
        }
    }

    public void DestroyItemText()
    {
        // KILL KILL KILL KILLL
        itemName.text = null;
        itemDesc.text = null;
        modifiers.Clear();
        desc = null;
        panelSize = 0;
    }

    public string ModifiersList(Equipment item)
    {
        //creates dictionary with all datas
        modifiers.Add("Damage : ", item.damageModifier);
        modifiers.Add("Armor : ", item.armorModifier);
        modifiers.Add("Agility : ", item.agilityModifier);
        modifiers.Add("Dexterity : ", item.dextertyModifier);
        modifiers.Add("Strength : ", item.strengthModifier);
        modifiers.Add("Intelligence : ", item.intelligenceModifier);
        modifiers.Add("Stamina : ", item.staminaModifier);

        foreach (KeyValuePair<string, int> entry in modifiers)
        {
            // check dictionnary for non-null values
            if (entry.Value > 0)
            {
                // adds values to the description string, increases size of panel
                desc += entry.Key + entry.Value + "\n";
                panelSize++;
            }
        }
        return desc;
    }
}
