﻿using UnityEngine;

public class ItemSpawnButton : Interactable
{
    public GameObject leatherShoulderOriginal;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Action"))
        {
            Interact();
        }
    }


    public override void Interact()
    {
       
        if (InteractionAvailable)
        {
            Debug.Log("Button");
            SpawnItem();
            ActionText.GetComponent<ShowText>().TxtDisable();
        }
        
    }

    public void SpawnItem()
    {
        GameObject leatherShoulderClone = Instantiate(leatherShoulderOriginal);
    }


    #region text management
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            ShowPickupMessage();
            InteractionAvailable = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        // Debug.Log(gameObject + "End Collision with " + item.name);
        HidePickupMessage();
        InteractionAvailable = false;
    }

    private void ShowPickupMessage()
    {
        ActionText.GetComponent<ShowText>().UseTxtEnable(gameObject.name);
    }

    private void HidePickupMessage()
    {
        ActionText.GetComponent<ShowText>().TxtDisable();
    }
    #endregion text management

}
