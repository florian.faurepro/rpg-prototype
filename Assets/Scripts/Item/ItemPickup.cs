﻿using UnityEngine;

public class ItemPickup : Interactable
{
    public Equipment item;
    public string actionKey;

    //public GameObject canvas;
    public InventoryUI invUI;

     void Start()
    {
        //canvas = GameObject.FindGameObjectWithTag("ActionText");
        invUI = canvas[0].GetComponent<InventoryUI>();
    }
    void Update()
    {
        // detects input from action button
        if(Input.GetButtonDown("Action") && InteractionAvailable == true
            && invUI.bagOpen == false && invUI.characterSheetOpen == false)
        {
            if (InteractionAvailable)
            {
                Pickup();
                actionTMP.GetComponent<ShowText>().TxtDisable();
            }
        }
    }

    public void Pickup()
    {
        //Debug.Log("Picking up " + item.name);
        Inventory.instance.Add(item);
        Destroy(gameObject);
    }

    #region text management
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            ShowPickupMessage();
            InteractionAvailable = true;       
        }
    }
    private void OnTriggerExit(Collider other)
    {
        HidePickupMessage();
        InteractionAvailable = false;
    }
    private void ShowPickupMessage()
    {
        actionTMP.GetComponent<ShowText>().ItemTxtEnable(item.name);
    }
    private void HidePickupMessage()
    {
        actionTMP.GetComponent<ShowText>().TxtDisable();
    }
    #endregion text management

}
