﻿using UnityEngine;
using TMPro;

public class ShowText : MonoBehaviour
{
    [SerializeField] 
    TextMeshProUGUI actionText;
    GameObject[] canvas;

    // creates an array of every gameobject tagged "Actiontext"
    private void Awake()
    {
        canvas = GameObject.FindGameObjectsWithTag("ActionText");
        foreach (GameObject go in canvas)
        {
            // sets actiontext var to TMPtext "Action Text"
            if (go.GetComponentInChildren<TextMeshProUGUI>())
            {
                actionText = go.GetComponentInChildren<TextMeshProUGUI>();
            }
        }
        actionText.enabled = false;
    }

    // enables the text for item pickup
    public void ItemTxtEnable(string interactableName)
    {
        actionText.enabled = true;
        actionText.text = "Press 'Action' to pickup " + interactableName + ".";        
    }

    // enables the text for usable object
    public void UseTxtEnable(string interactableName)
    {
        actionText.enabled = true;
        actionText.text = "Press 'Action' to use " + interactableName + ".";
    }

    public void TalkTxtEnable(string interactableName)
    {
        actionText.enabled = true;
        actionText.text = "Press 'Action' to talk to " + interactableName + ".";
    }

    // hides text
    public void TxtDisable()
    {
        actionText.enabled = false;
    }

}
