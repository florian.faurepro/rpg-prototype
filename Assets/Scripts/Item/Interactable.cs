﻿using UnityEngine;
using TMPro;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public GameObject ActionText;
    public GameObject[] canvas;
    public TextMeshProUGUI actionTMP;
    public bool InteractionAvailable = false;

    // creates an array of every gameobject tagged "Actiontext"
    private void Awake()
    {
        canvas = GameObject.FindGameObjectsWithTag("ActionText");
        foreach (GameObject go in canvas)
        {            
            // sets actiontext var to TMPtext "Action Text"
            if (go.GetComponentInChildren<TextMeshProUGUI>())
            {
                actionTMP = go.GetComponentInChildren<TextMeshProUGUI>();
            }
        }
        actionTMP.enabled = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius); 
    }
    
    public virtual void Interact ()
    {

    }
}
