﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Epuipment", menuName ="Inventory/Equipment")]
public class Equipment : Item
{
    public EquipmentSlot equipSlot;
    public EquipmentType equipType;

    public SkinnedMeshRenderer mesh;
   
    public int damageModifier;
    public int speedModifier;
    public int armorModifier;
    public int vitalityModifier;
    public int staminaModifier;
    public int strengthModifier;
    public int agilityModifier;
    public int dextertyModifier;
    public int intelligenceModifier;
    public bool identified = false;

    public override void Use()
    {
        //if (identified == false)        
        //    Randomize(10, "warrior", 3);        
        //else
        //{
            base.Use();
            EquipmentManager.instance.Equip(this);
            RemoveFromInventory();
        //}
    }

    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }

    //public void Randomize(int lvl, string type, int rarity)
    //{
    //    if (type == "warrior")
    //    {
    //        staminaModifier = Random.Range(0, lvl);
    //        vitalityModifier = Random.Range(0, lvl);
    //        strengthModifier = Random.Range(0, lvl);
    //        dextertyModifier = Random.Range(0, lvl);
    //    }
    //    identified = true;
    //}
}

public enum EquipmentSlot { Head, Chest, Hands, Feet, LeftHand, Face, Shoulders, Legs, RightHand, Consumable }
public enum EquipmentType { Armor, OneHand, TwoHands, SpellBook, Shield, Potion }