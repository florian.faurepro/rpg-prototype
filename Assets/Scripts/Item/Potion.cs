﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Potion", menuName ="Inventory/Potion")]
public class Potion : Equipment
{    
    public PlayerStats stats;
    public GameObject player;

    public float heal;

    private void Awake()
    {
        
    }

    public override void Use()
    {
        if(PlayerStats.instance.Heal(heal) == true)
            RemoveFromInventory();
    }
}