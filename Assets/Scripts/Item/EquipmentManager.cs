﻿using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    #region Singleton
    public static EquipmentManager instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public bool isArmed1H;
    public bool isArmed2H;
    public bool isArmedShield;
    public bool isArmedSpellBook;

    public Equipment[] defaultItems;
    public Equipment[] currentEquipment;
    public SkinnedMeshRenderer[] currentMeshes;
    public SkinnedMeshRenderer targetMesh;

    public CharacterPanel charpan;
    
    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    public Inventory inventory;

    // initiates the variables, gets reference to inventory and player mesh
    private void Start()
    {
        inventory = Inventory.instance;
        //charpan = CharacterPanel.instance;

        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
        currentMeshes = new SkinnedMeshRenderer[numSlots];

        //GameObject cp = GameObject.FindGameObjectWithTag("Charpan");
        //charpan = cp.GetComponent<CharacterPanel>();
        

        EquipDefaultItems();
    }

    // receive a new item and equips it
    public void Equip(Equipment newItem)
    {
        // finds out what slot the item fits in
        int slotIndex = (int)newItem.equipSlot;

        // gets the item previously equiped in the same slot
        Equipment oldItem = Unequip(slotIndex);
        

        if (onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }

        //  sets the received item in the corresponding slot
        currentEquipment[slotIndex] = newItem;

        // instantiates the corresponding mesh
        SkinnedMeshRenderer newMesh = Instantiate<SkinnedMeshRenderer>(newItem.mesh);

        // adds the new mesh on the player's mesh, manages it differently if the new item is a weapon
        if (newItem.equipSlot == EquipmentSlot.RightHand)
        {
            newMesh.rootBone = targetMesh.bones[22];
            newMesh.transform.parent = targetMesh.transform;
            currentMeshes[slotIndex] = newMesh;

            if (newItem.equipType == EquipmentType.OneHand)
                isArmed1H = true;
            if (newItem.equipType == EquipmentType.TwoHands)
                isArmed2H = true;
        }
        else
        {
            newMesh.bones = targetMesh.bones;
            newMesh.transform.parent = targetMesh.transform;
            newMesh.rootBone = targetMesh.rootBone;
            currentMeshes[slotIndex] = newMesh;
        }
        charpan.Add(newItem, slotIndex);
    }

    // Equips the default items if nothing else
    void EquipDefaultItems()
    {
        foreach (Equipment item in defaultItems)
        {
            Equip(item);
        }
    }

    // removes item from mesh and equipmed item
    public Equipment Unequip(int slotIndex)
    {
        // checks if there is something in the slot
        if(currentEquipment[slotIndex] != null )
        {
            // checks if there is a mesh in the slot
            if(currentMeshes[slotIndex] != null)
            {
                // destroys the targetted mesh
                Destroy(currentMeshes[slotIndex].gameObject);
            }
            // set olditem then adds it in inventory 
            Equipment oldItem = currentEquipment[slotIndex];
            // (the switch between equipped and in inventory is finished at this point)
            inventory.Add(oldItem);

            // updates the isArmed1H variable if a weapon is unequiped 
            // though when a weapon is unequiped the animator doesn't update the animation
            if (oldItem.equipSlot == EquipmentSlot.RightHand && oldItem.equipType == EquipmentType.OneHand )
                isArmed1H = false;
            if(oldItem.equipSlot == EquipmentSlot.RightHand && oldItem.equipType == EquipmentType.TwoHands )            
                isArmed2H = false;
             
                
            
            // updates the equipment manager's slot to empty
            currentEquipment[slotIndex] = null;

            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
            }
            // tells  the character panel to removes the oldItem icon  
            charpan.Remove(slotIndex);
            return oldItem;
        }
        return null;
    }

    // use unequip() on every equipment slot
    public void UnequipAll()
    {
        for (int i = 0; i < currentEquipment.Length; i++)
        {
            Unequip(i);
        }   
        
        EquipDefaultItems();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
            UnequipAll();
    }

}
