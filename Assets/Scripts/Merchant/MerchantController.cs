﻿using UnityEngine;

public class MerchantController : Interactable
{
    public Inventory inventory;
    public GameObject MerchUI;
    public bool MerchantUIOpen = false;

    //public GameObject canvas;
    public InventoryUI invUI;

    //public MerchantSlot[] merchSlot;
    public ShowText txt;

    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<ShowText>();
        //canvas = GameObject.FindGameObjectWithTag("ActionText");
        invUI = canvas[0].GetComponent<InventoryUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Action") && InteractionAvailable == true 
            && invUI.bagOpen == false && invUI.characterSheetOpen == false)
        {
            // display merchantUI
            // set selection position
            // close player inventory
            MerchantUIOpen = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            txt.TalkTxtEnable(transform.name);
            InteractionAvailable = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            txt.TxtDisable();
            InteractionAvailable = false;
        }
    }

}
