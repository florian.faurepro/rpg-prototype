﻿using UnityEngine;
using UnityEngine.AI;
using System;

public class EnemyController : MonoBehaviour
{
    private Vector3 previousPosition;

    public float stoppingDistance = 5f;
    public float lookRadius = 10f;
    public float attackRadius = 2f;
    public float speedPercent;
    public float curSpeed;
    public bool mobile = true;

    public NavMeshAgent agent;
    EnemyStats stats;
    Transform target;
    PlayerMotor playerMotor;
    //public GameObject[] defenseTris;

    public Transform attackPoint;
    public float attackRange = 1f;
    public LayerMask playerLayers;
    public float nextAttackTime;

    protected Animator animator;

    // sets the variables
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        playerMotor = target.GetComponentInChildren<PlayerMotor>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        stats = GetComponent<EnemyStats>();
    }

    // Update is called once per frame
    void Update()
    {
        DetectPlayer();
        InAttackRangeDetection();
    }

    // tries to find player in range 
    public void DetectPlayer()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        // if enemy alive, player is in range and player is not discreet
        if (distance <= lookRadius && playerMotor.isDiscreet == false && stats.isAlive == true && mobile == true)
        {
            GoToPlayer(distance);
        }
        
        // if enemy alive, player is in close range 
        else if (distance <= (lookRadius / 3) && stats.isAlive == true && mobile == true)
        {
            GoToPlayer(distance);
        }
    }

    // tries to face the target (got to add condition to check if enemy is not attacking)
    void FaceTarget()
    {
        // get the target direction by comparing and normalizing transforms
        Vector3 direction = (target.position - transform.position).normalized;

        // defines the rotation to adopt by using direction to create a Quaternion
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));

        // sets the rotation previously defined 
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 3f); 
    }

    public void GoToPlayer(float distance)
    {
        // sets destination of NavMeshAgent
        agent.SetDestination(target.position);

        // this block calculates the agent speed to apply it to the movement animation blend
        Vector3 curMove = transform.position - previousPosition;
        curSpeed = curMove.magnitude / Time.deltaTime;
        previousPosition = transform.position;
        speedPercent = curSpeed / agent.speed;
        animator.SetFloat("SpeedPercent", speedPercent);

        if (distance <= stoppingDistance && mobile == true)
        {
            FaceTarget();
        }
    }

    // attacks if player inside attack range
    public void InAttackRangeDetection()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= attackRadius && stats.isAlive == true)
        {
            Attack();
        }
    }

    // choose one attack randomly
    public void Attack()
    {
        if (Time.time > nextAttackTime)
        {
            int rand = UnityEngine.Random.Range(0, 200);

            if (rand >= 0 && rand <= 50)
            {
                ZombieSlap();
                nextAttackTime = Time.time + 5f;
            }
            if (rand >= 50 && rand <= 100)
            {
                ZombieDoubleSlap();
                nextAttackTime = Time.time + 4f;
            }            
        }
    }

    // plays animator trigger Attack1
    public void ZombieSlap()
    {
        animator.SetTrigger("Attack1");
    }

    // plays animator trigger Attack2
    public void ZombieDoubleSlap()
    {
        animator.SetTrigger("Attack2");
    }

    public void Damage(int multiplier)
    {
        // detect player(s)
        Collider[] playerInRange = Physics.OverlapSphere(attackPoint.position, attackRange, playerLayers);

        // damage player(s) in range
        foreach (Collider player in playerInRange)
        {
            // sets the Variables
            #region DamageVars
            int hitpoints = stats.attackPower * multiplier;
            float prevDistance = 0;
            string closer = "";
            bool damageFromBack = false;
            GameObject[] defenseTris = new GameObject[3];

            defenseTris[0] = player.transform.Find("DefensePointFront").gameObject;
            defenseTris[1] = player.transform.Find("DefensePointBackRight").gameObject;
            defenseTris[2] = player.transform.Find("DefensePointBackLeft").gameObject;
            #endregion DamageVars

            for (int i = 0; i < defenseTris.Length; i++)
            {
                float distance = Vector3.Distance(transform.position, defenseTris[i].transform.position);

                // set the first distance
                if(i == 0)
                {
                    prevDistance = distance;
                    closer = defenseTris[i].name;
                }
                // keep track of the closest point 
                else if (prevDistance > distance)
                {
                    closer = defenseTris[i].name;
                }
            }
            if (closer != "DefensePointFront")
                damageFromBack = true;

            // sends the amout of damage and the direction to the player's TakeDamage function
            player.GetComponent<PlayerStats>().TakeDamage(hitpoints, damageFromBack);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(attackPoint.transform.position, attackRadius);
    }
}
