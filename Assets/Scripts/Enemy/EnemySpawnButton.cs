﻿using UnityEngine;

public class EnemySpawnButton : Interactable
{
    public GameObject EnemyOriginal;
    public Transform spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Action"))
        {
            Interact();
        }
    }


    public override void Interact()
    {
       
        if (InteractionAvailable)
        {
            Debug.Log("Spawning Enemy");
            SpawnEnemy();
            ActionText.GetComponent<ShowText>().TxtDisable();
        }
        
    }

    public void SpawnEnemy()
    {
        GameObject EnemyClone = Instantiate(EnemyOriginal, spawnPoint.transform.position, Quaternion.Euler(0,0,0));
    }


    #region text management
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            ShowPickupMessage();
            InteractionAvailable = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        // Debug.Log(gameObject + "End Collision with " + item.name);
        HidePickupMessage();
        InteractionAvailable = false;
    }

    private void ShowPickupMessage()
    {
        ActionText.GetComponent<ShowText>().UseTxtEnable(gameObject.name);
    }

    private void HidePickupMessage()
    {
        ActionText.GetComponent<ShowText>().TxtDisable();
    }
    #endregion text management

}
