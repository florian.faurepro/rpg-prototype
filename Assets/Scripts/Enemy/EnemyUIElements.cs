﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyUIElements : MonoBehaviour
{
    public Slider slider; /*{ get; protected set; }*/
    public TextMeshProUGUI damageDetails;
    public Image targetImage;
    public Camera cameraToLookAt;

    private void Awake()
    {
        cameraToLookAt = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Camera>();
        slider = GetComponentInChildren<Slider>();
        //slider.gameObject.SetActive(false);
        //targetImage.gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 v = cameraToLookAt.transform.position - transform.position;

        v.x = v.z = 0.0f;
        slider.transform.LookAt(cameraToLookAt.transform.position - v);
        slider.transform.Rotate(0, 180, 0);

        targetImage.transform.LookAt(cameraToLookAt.transform.position - v);
        targetImage.transform.Rotate(0, 180, 0);

        damageDetails.transform.LookAt(cameraToLookAt.transform.position - v);
        damageDetails.transform.Rotate(0, 180, 0);
    }
    
    public void Activate()
    {
        slider.gameObject.SetActive(!slider.gameObject.activeSelf);
    }
}
