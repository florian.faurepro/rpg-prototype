﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;


// ************************************************************************************************************************
// This file is to be reviewed in order to delete useless parts. Seen it derives from CharacterStats, there may be doubles.
// ************************************************************************************************************************


public class EnemyStats : CharacterStats
{
    protected Animator animator;

    public bool isAlive { get; protected set; }
    public bool isLocked;
    public float rezTime;
    public bool isCrit;
    public float speed;
    public float healthPercent;

    public Image LockTargetImage;
    public Slider slider;
    public TextMeshProUGUI damageText;
    public EnemyController controller;
    public CapsuleCollider caps;

    public int totalDamage;
    public float damageTextEnableTime;

    public void Awake()
    {
        speed = GetComponent<NavMeshAgent>().speed;
        animator = GetComponentInChildren<Animator>();
        controller = GetComponent<EnemyController>();
        caps = GetComponent<CapsuleCollider>();

        InitiateStats();

        UpdateStats();

        currentHealth = maxHealth;
        healthPercent = (currentHealth / maxHealth);
        attackPower = strength.GetValue() + (agility.GetValue() / 2) + dexterity.GetValue();
        critChances = (strength.GetValue() / 5) + (agility.GetValue() / 3) + (dexterity.GetValue() / 2);

        Instantiate(damageText, transform.position, Quaternion.identity, transform);

        isAlive = true;
    }

    private void Update()
    {
        slider.value = healthPercent;

        if (Time.time >= damageTextEnableTime)
            HideDamageText();

        LockImage(isLocked);
    }

    
    public void TakeDamage(int damage, bool isCrit, bool isMagic, bool backstab)
    {
        // adds 5 seconds to damageText presence time
        damageTextEnableTime = Time.time + 5f;


        // if damages comes from the back, doubles
        if (backstab)
            damage += damage;
        
        // protection reduces incoming damage
        damage -= protection;
        
        if (damage < 0)
            damage = 0;

        // actual incoming damage déduced from health
        currentHealth -= damage;

        Debug.Log("Enemy took " + damage + " damage." );

        // get healthPercentage
        healthPercent = (currentHealth / maxHealth);

        // animation is played
        animator.SetTrigger("Hit");

        // check for death
        if (currentHealth <= 0)
            Die();

        ShowDamageText(damage, isCrit, isMagic);
    }

    public void ShowDamageText(int damage, bool isCrit, bool isMagic)
    {        
        totalDamage += damage;
        if (isCrit)
            damageText.color = Color.yellow;
        else
            damageText.color = Color.white;
            
        damageText.enabled = true;
        slider.gameObject.SetActive(true);
        damageText.text = totalDamage.ToString();
    }

    public void HideDamageText()
    {
        totalDamage = 0;
        damageText.enabled = false;
        if (!isLocked)
            slider.gameObject.SetActive(false);
    }

    public override void Die()
    {
        Debug.Log(transform.name + " dieded.");

        // -------- play death animation
        animator.SetBool("IsDead", true);

        // -------- render inactive
        isAlive = false;
        controller.enabled = false;
        caps.enabled = false;
        controller.agent.enabled = false;
    }    

    // if respawn times over reset deaths variables, animator and HP
    public void Rez()
    {       
        if (Time.time >= rezTime)
        {
            currentHealth = maxHealth;
            animator.SetBool("IsDead", false);
            isAlive = true;
            healthPercent = (currentHealth / maxHealth);
        }
    }

    public void LockImage(bool isLocked)
    {
        LockTargetImage.gameObject.SetActive(isLocked);
    }
}
