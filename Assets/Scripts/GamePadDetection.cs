﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePadDetection : MonoBehaviour
{
    #region Singleton
    public static GamePadDetection instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int Xbox_One_Controller { get; protected set; }
    public int PS4_Controller { get; protected set; }

    public int miceNKeys { get; protected set; }


    // Start is called before the first frame update
    void Start()
    {
        miceNKeys = 1;
    }

    // Update is called once per frame
    void Update()
    {
        string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            if (names[x].Length == 19)
            {
                PS4_Controller = 1;
                Xbox_One_Controller = 0;
                miceNKeys = 0;
            }
            if (names[x].Length == 33)
            {
                PS4_Controller = 0;
                Xbox_One_Controller = 1;
                miceNKeys = 0;
            }
            else
            {
                miceNKeys = 1;
                PS4_Controller = 0;
                Xbox_One_Controller = 0;
            }
        }
    }
}
